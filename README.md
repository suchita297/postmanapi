# PostmanAPI



##introduction
In this project doing postman used for to automate different rest APIs.with capability of pm.library,jawa script base pm library,testing capability of postmon along with different REST methods post,put,patch,get,delete. i have used  data driven test, request chaining,also i have used capabilities of postman to environmentfiles,data files.
i have also used collection runner, newman runer.

Description
Postman project.
First to download and install postman from browser.

 Created new collections.after that add a request within a collection with request type(post,put,patch,get,delete etc.) add request of these methods using (https://reqres.in/).

created folders according to methods for add new request of rest API.
add endpoint of the API in to URL field.give the name of that request according to our test.add the request body under the body parameters below the URLS folder.write test cases relevant to test request method like post(to create),put(update),patch(update/replace),get(read) under test case parameter.

Save and send request postman will display response according to request status code,body,and test case.
 environment variables  add endpoint and send request.postman give the relevant response to request.
 In data driven test for restAPI method set the variables with values. by using random variables is pre_request script written in jawa script and run before the request.By including code in the Pre-request Script tab for a request, collection, or folder, you can carry out pre-processing such as setting variable values, parameters, headers, and body data.
 To created Data files with json data file(with json formate),and excel file.
enter data in body of request postman responded as acoording status code,bady and header parameter.


In newman postman collection and environment export in file expoler.this export data used for newman to generate htmp report with detail explaination of our postman collection.we also prepared htmletra report. Install newman.run the newman cammnad with proper syntax(newman run locationname of collection\name of collection) -e (location name of environment\name of environment)
Also add the data file in that syntax with(-d location and name of data file)and run the cammand gove the newman response with running of entire postman collection.creating report of collection by using cammand(--reporters-reporter name-export(location of save report) report file).

Installation
Important installation used to run postman
postman install(https://www.postman.com/)
newman install(https://learning.postman.com/docs/collections/using-newman-cli/installing-running-newman/)
for rest API method reference(https://reqres.in/)